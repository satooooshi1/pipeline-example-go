# pipeline-example-go

This is a sample golang project to demonstrate the integration with rancher pipeline.

## Building

`go build -o ./bin/hello-server`

## Running

`./bin/hello-server`

